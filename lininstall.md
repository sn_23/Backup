## Install vim
```sudo apt install vim```

## Terminator
```sudo apt install terminator```

## Hack Nerd Font
[Hack Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hack/Regular/complete)

## GTKVim
```sudo apt install vim-gtk3```

## i3wm
```sudo apt install i3```

## curl
```sudo apt install curl```

## Plug Plugin Manager
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

## Git
```sudo apt install git```

## g++
```sudo apt install g++```

## feh
```sudo apt install feh```

## All
```
sudo apt install vim && apt install terminator && apt install vim-gtk3 && apt install i3 && apt install curl && apt install git && apt install g++ && apt install feh
```


