#include<bits/stdc++.h>
using namespace std;

#define ll long long
#define po pop_back // remove elements from a vector from back
#define pb push_back // push the element into a vector from back 
#define mp make_pair
#define pii pair<int,int>
#define vi vector<int>
#define mii map<int,int>
#define pqb priority_queue<int>
#define pqs priority_queue<int,vi,greater<int> >
#define fast ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define OJ freopen("input.txt","r",stdin); freopen("output.txt","w","stdout");
#define gcd long long gcd(long long a,long long b){if(b ==0){return a;}return gcd(b,a%b);}

